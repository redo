package:
	grep ^Version: DEBIAN/control | cut -d' ' -f 2 > usr/share/redo/VERSION
	rm -rf redobackup-deb redobackup_*.deb
	git archive --prefix=redobackup-deb/ HEAD | tar -xf -
	rm -rf redobackup-deb/etc/skel
	rm -rf redobackup-deb/etc/default
	rm -rf redobackup-deb/etc/rcS
	rm -rf redobackup-deb/etc/rc.local
	rm -rf redobackup-deb/config
	cp usr/share/redo/VERSION redobackup-deb/usr/share/redo/VERSION
	dpkg-deb -b redobackup-deb
	dpkg-name redobackup-deb.deb
	rm -rf redobackup-deb

live: package
	lb config -b iso-hybrid --memtest memtest86+ --bootloaders syslinux,grub-efi \
	  --image-name "redobackup_$$(cat usr/share/redo/VERSION)" \
	  --iso-application "redobackup $$(cat usr/share/redo/VERSION)" \
	  --iso-preparer "live-build $$(lb --version)" \
	  --iso-publisher "$$USER@$$HOSTNAME" \
	  --iso-volume "Redobackup $$(cat usr/share/redo/VERSION) Debian"
	rm -rf config/packages.chroot/redobackup_*.deb
	cp -a redobackup_*.deb config/packages.chroot/
	lb clean
	lb build
	mkdir -p media
	mv redobackup*.iso media
	lb clean --binary
	lb config -b netboot --bootloaders syslinux
	lb build
	mv redobackup*.netboot.tar media
