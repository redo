-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: icewm
Binary: icewm-common, icewm, icewm-lite, icewm-experimental
Architecture: any
Version: 2.0.0-2
Maintainer: Eduard Bloch <blade@debian.org>
Standards-Version: 4.5.0
Vcs-Git: https://github.com/Code7R/icewm.git
Build-Depends: debhelper (>= 9), libxrender-dev, gettext, libxft-dev (>> 2.1.1) | libxft2-dev, libsndfile1-dev, libao-dev, libsm-dev, libx11-dev, libxext-dev, libxinerama-dev, libxrandr-dev, x11proto-core-dev | x-dev, dpkg-dev (>= 1.16.1~), cmake, asciidoctor, libglib2.0-dev, libfribidi-dev, librsvg2-dev, libxcomposite-dev, libxdamage-dev, pandoc, libimlib2-dev
Build-Conflicts: libesd0-dev, libttf-dev
Package-List:
 icewm deb x11 optional arch=any
 icewm-common deb x11 optional arch=any
 icewm-experimental deb x11 optional arch=any
 icewm-lite deb oldlibs optional arch=any
Checksums-Sha1:
 b727b21234c33a84747f5c0d465c9870bda981b8 1403500 icewm_2.0.0.orig.tar.xz
 d681faece4173d2c74277732f0bce518ec7bdb2c 64572 icewm_2.0.0-2.debian.tar.xz
Checksums-Sha256:
 29b41e7b255ae0e8bf32e65fd4ebc330093b786dbbf92bd2a5910dcc98d47a50 1403500 icewm_2.0.0.orig.tar.xz
 a2d171559c6a9f9d78328d799547cee76a47462ce8c48f62743df89bd6a49bc6 64572 icewm_2.0.0-2.debian.tar.xz
Files:
 5f8563cc3adda3212eb4a2ac2d881d50 1403500 icewm_2.0.0.orig.tar.xz
 703df350950c818b821901e99fbe5e0c 64572 icewm_2.0.0-2.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCAAdFiEEZI3Zj0vEgpAXyw40aXQOXLNf7DwFAl/Xl/MACgkQaXQOXLNf
7Dz3LA//QywJkdn7+aqom7MDM5qXs/jE2ig4nqtfaRxgZ0NWh3Pn8UnTrSrwi/Pt
UMM2AjKdOwH/JhcjRe+YU+hLb5tcUlokjFJo+AbzIHgJ4DQ2lqcnLGziF2kJJH0I
kZdhBKv8QbTuMsKaoj/phNroqIM50Quahovq20J4GGvynLKReHriL7CNpRj5cj+J
5SIgO/6dgMZda/Nw/f8YJK0NYpqoor2neYoYD6dj4MnWIxvXNT7qPIwqTcBIcqS6
xoWMGMaVQdPPj6sA8pJ5VeUimSm8gVWaM0gnnkcnl9+yNQ5hYnP74dK9mwFx+xpp
FJPXq8b9xuX+ZD5pkQYqmK72mYmBIOxM7x4eSGLKOb3HPbDpzJWvBIugNkg7aiB2
Ushl09kMfSExmYrRfEDB9IYR3++cs/FJJ5VHmoztyfwVdSWS5FHvHGCEjSYrZnLA
XkWelTuJk3wO+PJ5mU4NWvUxy9Lk1Y53XPSJuZohuAEOSlC0amRFPaQx24hx37Ml
YL5kmtwiAqGL+XgiwTHTQvKIGBowKkg5KHmqGu5E6+CvM1cPaAkd4GkOahVkfYYI
uIWFxCPj2yv0Ed12EAzgX8Y34mt82V+VZSicbqLqgmRWror9JIi3XUNcVquQBwxk
RpA+JVE7jUTCI+IaWZs1EpH6iHNVllNumht8EpSGqCqKaFrGuSk=
=Gn3p
-----END PGP SIGNATURE-----
